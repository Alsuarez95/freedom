
public class SubaruOutback extends ACar {
	
	public SubaruOutback(int inYear, int inMilage)
	{
		super("Subaru", "Outback", inYear);
		setMilage(inMilage);
	}

}
