
public class HondaAccord extends ACar {
	
	public HondaAccord(int inYear, int inMilage)
	{
		super("Honda", "Accord", 2016);
		setMilage(inMilage);
	}
}
