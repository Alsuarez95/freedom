
public abstract class ACar implements ICar {
	
	private String make;
	
	private String model;
	
	private int year;
	
	private int milage;
	
	
	public ACar(String inMake, String inModel, int inYear)
	{
		make = inMake;
		model = inModel;
		year = inYear;
	}
	
	protected void setMilage(int inMIlage)
	{
		milage = inMilage;
	}
	
	@Override
	public String getMake()
	{
		return make;
	}
	
	@Override
	public String getModel()
	{
		return model;
	}
	
	@Override
	public int getMilage()
	{
		return milage;
	}
	
	@Override
	public int getYear()
	{
		return year;
	}

}
